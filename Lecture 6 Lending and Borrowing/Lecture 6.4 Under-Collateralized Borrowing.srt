﻿1
00:00:00,320 --> 00:00:01,425
欢迎同学们回来

2
00:00:02,000 --> 00:00:05,328
我们现在将深入到欠额抵押贷款

3
00:00:05,328 --> 00:00:09,750
它使你能够进行超过2倍杠杆的操作

4
00:00:10,559 --> 00:00:12,719
跟超额抵押贷款差不多

5
00:00:12,719 --> 00:00:14,559
这里又有一个借款者，对吧

6
00:00:14,559 --> 00:00:18,960
举例，借方可以抵押以太币并借Dai

7
00:00:18,960 --> 00:00:21,279
这里也有一个金库

8
00:00:21,279 --> 00:00:24,175
借方把他的资产存入这个金库

9
00:00:24,800 --> 00:00:27,439
然而现在的不同之处在于

10
00:00:27,439 --> 00:00:30,080
这里（借出的）Dai的价值，即债务金额

11
00:00:30,080 --> 00:00:32,079
可以超过(抵押的)以太币的价值

12
00:00:32,079 --> 00:00:34,559
即这个案例中当作抵押品的价值

13
00:00:34,559 --> 00:00:40,350
而在欠额抵押借贷中非常关键的一点是

14
00:00:40,625 --> 00:00:44,160
借方所借的Dai以及所抵押的抵押品

15
00:00:44,160 --> 00:00:47,100
只能受限于用在预先设计好的智能合约

16
00:00:47,490 --> 00:00:51,050
因此你不能自己编写智能合约

17
00:00:51,500 --> 00:00:55,258
将你的债务发送到这个合约中

18
00:00:55,258 --> 00:01:03,175
你必须使用借贷平台自身预定义，预先设计的智能合约

19
00:01:03,735 --> 00:01:05,815
因此，这些通常是“收益农场”（farming）智能合约 （译注：后面可以看到只能投向一些流动性池）

20
00:01:05,840 --> 00:01:10,159
所以如果你想利用借贷获得收益

21
00:01:10,159 --> 00:01:12,750
那显然这是需要考虑的事情

22
00:01:13,050 --> 00:01:15,725
但如果你想对你的债务进行任意操作

23
00:01:16,050 --> 00:01:19,649
那么它可能不会为你提供任何帮助

24
00:01:20,400 --> 00:01:22,560
所以在这里非常重要的一点是

25
00:01:22,560 --> 00:01:29,375
金库在债务头寸的生命周期内的任何时间点 都保持对所有资产的控制

26
00:01:30,960 --> 00:01:32,799
接下来举个例子

27
00:01:32,799 --> 00:01:34,880
这个截图是Alpha Homora

28
00:01:34,880 --> 00:01:36,960
或者说是Alpha Homora的面板

29
00:01:36,960 --> 00:01:44,375
在这里有一个价值为1900美元的抵押品

30
00:01:44,425 --> 00:01:49,350
然后咱有一小笔债务，350美元

31
00:01:49,600 --> 00:01:52,400
并且作为奖励收到一些平台的代币

32
00:01:52,500 --> 00:01:56,550
这是，这是基于代币的特定APY

33
00:01:56,925 --> 00:02:02,700
还可以看到这里，已经开立的仓位

34
00:02:02,775 --> 00:02:06,400
这是在Uniswap的Dai池中开立的仓位

35
00:02:07,200 --> 00:02:10,850
好的，你可以添加一些额外的元素

36
00:02:10,925 --> 00:02:13,005
或者你愿意的话也可以关闭这个仓位

37
00:02:15,725 --> 00:02:16,925
在Alpha Homora中

38
00:02:17,000 --> 00:02:18,875
你还可以看到所有的开放仓位

39
00:02:18,975 --> 00:02:24,400
你可以在他们的网页上检查这些仓位的情况

40
00:02:24,400 --> 00:02:26,640
像是债务比例是什么

41
00:02:26,640 --> 00:02:29,900
有些东西我认为你可能有兴趣对它们做进一步了解

42
00:02:29,950 --> 00:02:30,879
你可以探索抵押信用

43
00:02:30,879 --> 00:02:33,440
借贷信用

44
00:02:33,475 --> 00:02:35,475
以及总体抵押品价值

45
00:02:35,760 --> 00:02:39,950
请注意，有一些池是稳定币池

46
00:02:40,160 --> 00:02:42,319
就像这个 （译注：老师圈住的是Curve的池子 参见第五讲第四节）

47
00:02:42,319 --> 00:02:47,050
还有一些是更具投机性的代币池 (译注：老师圈住了一个Uniswap的ETH对CRV的流动性池子)

48
00:02:47,175 --> 00:02:50,525
它们并不稳定，也不应该稳定

49
00:02:52,640 --> 00:02:55,500
所以我们对Alpha Homora进行了数据爬取

50
00:02:56,075 --> 00:02:57,825
实际上，从它成立开始

51
00:02:57,920 --> 00:03:00,720
Alpha Homora于2020年10月开始运营

52
00:03:00,720 --> 00:03:04,125
我们一直爬取它的智能合约数据直到2021年8月

53
00:03:04,125 --> 00:03:08,250
我们发现大约有3800名借款人

54
00:03:08,900 --> 00:03:16,175
其中我们发现有10430个杠杆头寸在我们测量结束时已经关闭

55
00:03:16,200 --> 00:03:21,325
因此，我们基本上是在搜索所有开放的借贷头寸

56
00:03:21,519 --> 00:03:25,625
以便能够计算它们最终的APY

57
00:03:25,825 --> 00:03:28,705
即它们最终的回报，以及这（借款行为）是否值得

58
00:03:29,200 --> 00:03:31,599
我们得到了一些有趣的数据

59
00:03:31,599 --> 00:03:38,375
例如，我们发现平均杠杆倍数大约是2.x到3.x

60
00:03:38,640 --> 00:03:41,120
分别是在相应的版本上

61
00:03:41,120 --> 00:03:43,500
它有两个版本

62
00:03:43,850 --> 00:03:48,125
主要区别在于版本2允许使用更多的资产

63
00:03:48,250 --> 00:03:51,275
而不仅仅是以太币

64
00:03:52,159 --> 00:03:56,150
我们还发现了一个颇具趣味的事实

65
00:03:56,200 --> 00:03:59,500
稳定币的平均杠杆倍数相当高

66
00:03:59,600 --> 00:04:04,000
稳定币的平均杠杆倍数几乎是5.4

67
00:04:04,000 --> 00:04:09,050
这是有道理的，因为稳定币本质上波动性较低

68
00:04:09,200 --> 00:04:12,775
因此，承担更高杠杆的风险较小

69
00:04:13,680 --> 00:04:17,375
被清算的风险自然也不是那么大

70
00:04:18,399 --> 00:04:21,350
那么，借方是如何选择杠杆倍数的呢？

71
00:04:21,475 --> 00:04:33,125
我们根据这些图结果，绘制 了基于稳定币、部分稳定币 或非稳定币的数据(译注：左侧绿色为稳定币)

72
00:04:33,225 --> 00:04:36,750
稳定币对是任意一对都是稳定币

73
00:04:36,800 --> 00:04:43,100
部分稳定币对是其中有一个是稳定代币，但另一个是非稳定币

74
00:04:43,120 --> 00:04:45,850
非稳定币对就是都是非稳定的代币

75
00:04:46,240 --> 00:04:50,850
同学们可以在这里再次看到杠杆倍率的分布情况

76
00:04:50,925 --> 00:04:59,975
稳定币的杠杆倍数要明显高于部分或非稳定币的杠杆倍数

77
00:05:00,080 --> 00:05:01,759
它们或多或少有些相似是吧

78
00:05:01,759 --> 00:05:03,680
它们基本上有相同的用户

79
00:05:03,680 --> 00:05:06,325
基本上选择相同的杠杆倍数

80
00:05:06,400 --> 00:05:09,475
一旦他们的篮子（投资）里有一个非稳定币（就会这么选择）

81
00:05:10,400 --> 00:05:12,800
现在，让我们看一下不同的平台 (译注：贷款投资流向的平台，不是借贷平台)

82
00:05:12,800 --> 00:05:18,950
在这个第二个图中，我们区分了不同的平台

83
00:05:19,200 --> 00:05:22,241
我们展示了Curve、Balancer、Sushi Swap和Uniswap

84
00:05:22,800 --> 00:05:26,550
我们可以发现，对于Curve

85
00:05:26,700 --> 00:05:28,560
图中红色的那个

86
00:05:28,560 --> 00:05:30,625
杠杠率相当高

87
00:05:30,960 --> 00:05:34,675
这是有道理的，因为Curve支持最稳定的代币池

88
00:05:35,100 --> 00:05:38,575
但Uniswap也支持一些稳定代币池

89
00:05:38,850 --> 00:05:42,600
所以非常有意思，在Uniswap上看到有一些高杠杆头寸

90
00:05:43,199 --> 00:05:50,300
此外，Sushi Swap在这里正在占据相当大一部分头寸

91
00:05:50,850 --> 00:05:55,675
这些数据基于Alpha Homora版本2的杠杆倍数

92
00:05:55,840 --> 00:05:59,475
我们调查了大约2500个头寸

93
00:06:01,680 --> 00:06:05,475
有同学可能会问，APY和杠杆究竟是什么关系呢？

94
00:06:05,680 --> 00:06:09,000
为什么我不总是选择最高可能的杠杆呢？

95
00:06:09,759 --> 00:06:18,250
在x轴上，我们绘制了我们发现的这些Alpha Homora头寸的初始杠杆

96
00:06:18,350 --> 00:06:24,850
也就是在你开启杠杆时的初始位置

97
00:06:25,039 --> 00:06:29,200
然后在关闭仓位后

98
00:06:29,475 --> 00:06:32,075
我们计算了APY，借方的APY

99
00:06:32,479 --> 00:06:36,050
同学们从可以在这里看到，情况并非总是乐观

100
00:06:36,325 --> 00:06:40,500
我们有相当多的情况，APY实际上是负数

101
00:06:40,720 --> 00:06:42,639
有些甚至可以说是显著的负数

102
00:06:42,639 --> 00:06:49,450
这里点的大小实际上表示了各个仓位的相对大小

103
00:06:49,919 --> 00:06:53,050
所以它越大，相对于其他仓位（的规模）就越大

104
00:06:53,280 --> 00:06:59,075
我们根据仓位的持续时间把它们进行了区分

105
00:06:59,225 --> 00:07:05,325
红点（译注：三角形）表示所有持续时间短于一天的仓位

106
00:07:05,759 --> 00:07:10,000
通过添加一个回归线，同学们可以看到

107
00:07:10,160 --> 00:07:16,675
持续越短以及杠杆越高

108
00:07:16,775 --> 00:07:18,925
APY就会变得越糟糕，是吧

109
00:07:19,125 --> 00:07:22,240
例如，对于7倍的杠杆的头寸

110
00:07:22,240 --> 00:07:29,725
从APY来说实际上是相当不行的

111
00:07:30,150 --> 00:07:33,100
不幸中的万幸是，这些仓位相对短期

112
00:07:33,360 --> 00:07:37,950
因此也可能是借方实际上迅速退出仓位

113
00:07:38,125 --> 00:07:41,125
以避免进一步的负面风险暴露

114
00:07:41,680 --> 00:07:46,025
但有趣的是，长期仓位（的表现）仍然是相当正面的

115
00:07:46,319 --> 00:07:49,919
呃，像这些都接近正收益

116
00:07:49,919 --> 00:07:53,199
因此至少具有相当大的变化

117
00:07:53,199 --> 00:07:58,650
我们还可以看到，一旦我们看看更高杠杆的头寸时

118
00:07:59,025 --> 00:08:02,300
负面数据点明显比正面数据点多

119
00:08:02,350 --> 00:08:03,840
正面数据点看起来几乎是空的

120
00:08:03,840 --> 00:08:07,625
然而如果我们看一下大约两倍杠杆的头寸

121
00:08:08,200 --> 00:08:11,475
那么在这个2倍至3倍杠杆范围内

122
00:08:11,625 --> 00:08:17,000
可以看到相对平均地分布了正收益和负收益的数据

123
00:08:17,440 --> 00:08:20,800
好的，我会留下这些数据，呃……

124
00:08:20,800 --> 00:08:25,150
我会把这些数据的进一步分析与解读的任务留给同学们

125
00:08:25,440 --> 00:08:29,600
同学们尽可以留言或者在课程的聊天室中讨论

126
00:08:29,725 --> 00:08:34,625
如果你们对这些可视化图表有任何进一步的想法

127
00:08:37,200 --> 00:08:38,000
那么

128
00:08:38,000 --> 00:08:42,575
有同学可能会问，为什么我们不选择最大化的杠杆？

129
00:08:42,880 --> 00:08:47,950
为什么我们不干脆总是选最大的杠杆倍率

130
00:08:48,250 --> 00:08:52,775
杠杆为什么在实践中不总能增加APY呢？

131
00:08:53,120 --> 00:08:59,900
实际上，杠杆倍数对许多不同的风险或事件会产生影响

132
00:09:00,000 --> 00:09:01,175
这些我们都可以看到

133
00:09:01,175 --> 00:09:05,825
如果只看收入，杠杆倍数（的增加）应该会增加您的收益

134
00:09:06,240 --> 00:09:14,675
然而因为要借更多的资金，所以借款利息也会增加

135
00:09:15,050 --> 00:09:17,675
而且可能相当高昂，这取决于市场状况

136
00:09:17,700 --> 00:09:19,760
它们可能是很关键的因素

137
00:09:19,760 --> 00:09:24,075
而且清算风险也会随着杠杆倍数的增加而增加

138
00:09:24,400 --> 00:09:27,800
这是可不是你想暴露其中的危险

139
00:09:28,000 --> 00:09:32,700
因为一旦你被清算，你的抵押品将以折扣价出售

140
00:09:32,850 --> 00:09:34,825
显然会产生损失

141
00:09:35,700 --> 00:09:39,525
就像自动做市商一样

142
00:09:39,650 --> 00:09:44,400
在Alpha Homora或超额……欠额抵押的低押贷款平台上

143
00:09:44,450 --> 00:09:47,150
您也会遇到无常损失 （译注：无常损失参见第五讲）

144
00:09:47,300 --> 00:09:51,090
而且这种无常损失有时可以是正的，有时可以是负的

145
00:09:51,600 --> 00:09:57,300
尤其是由于这些平台上可能发生的保证金交易类型

146
00:09:57,325 --> 00:10:00,975
这一点在本堂课没有太多时间深入探讨

147
00:10:01,075 --> 00:10:06,700
但我真的鼓励同学们去探索这些特定的风险

148
00:10:06,750 --> 00:10:09,229
并进一步加以理解

149
00:10:09,440 --> 00:10:12,850
随时可以联系我提出问题

150
00:10:14,560 --> 00:10:20,950
非常感谢同学们在这堂关于杠杆、欠额抵押借贷平台讲座中的仔细听讲

151
00:10:21,279 --> 00:10:25,200
我强烈建议同学们亲自尝试

152
00:10:25,450 --> 00:10:28,950
但请小心，你可能会遭受资金的重大损失

153
00:10:29,450 --> 00:10:33,575
尤其是如果你选择了过高的杠杆倍数

154
00:10:33,700 --> 00:10:37,575
也许较小的杠杆倍数是更安全的选择

155
00:10:37,650 --> 00:10:39,275
但这由同学们自己做主

