﻿1
00:00:00,240 --> 00:00:01,150
欢迎同学们回来

2
00:00:01,825 --> 00:00:05,961
这节课我们要深入探讨超额抵押借贷 （over collateralized borrowing）

3
00:00:07,200 --> 00:00:11,900
在超额抵押借贷中，我们有两个参与者，对吧

4
00:00:11,900 --> 00:00:16,250
在这有一个世界，智能合约管理着资产

5
00:00:16,720 --> 00:00:19,680
然后我们有一个借方

6
00:00:20,640 --> 00:00:25,013
他发行或创建债务

7
00:00:25,760 --> 00:00:26,480
那么

8
00:00:26,480 --> 00:00:29,551
在这个特定的例子中，借方抵押了

9
00:00:29,551 --> 00:00:30,905
例如以太币

10
00:00:31,199 --> 00:00:37,800
然后借了Dai，这是一个非常传统的创造稳定币的方式

11
00:00:37,800 --> 00:00:39,075
像是MakerDAO

12
00:00:39,550 --> 00:00:46,975
所以从本质上讲，我们这里有150%价值的以太币作为抵押

13
00:00:47,225 --> 00:00:51,000
然后我们可以铸造100%价值的Dai

14
00:00:51,680 --> 00:00:54,304
因此重要的是这里以太币的价值

15
00:00:54,304 --> 00:00:56,879
作为抵押品超过了Dai的价值，绝对价值上来说，对吧

16
00:00:56,879 --> 00:01:01,000
如果把它（价值）转换成像一个基础货币

17
00:01:01,520 --> 00:01:03,359
而且同样重要的是

18
00:01:03,359 --> 00:01:07,625
在这个例子中借方可以自由使用Dai

19
00:01:08,479 --> 00:01:13,200
实际上他可以将其转换回以太币或其他任何货币，借更多的资产

20
00:01:13,475 --> 00:01:18,100
因此在某种意义上说，它（借来的Dai）是一个非常非限制性的资产

21
00:01:19,200 --> 00:01:27,725
缺点是，显然借方在这里可以承担的杠杆永远不会超过2.x

22
00:01:27,725 --> 00:01:36,375
所以这可不是高度投机交易者或借方想要接受的模式

23
00:01:38,079 --> 00:01:40,850
举个例子吧，这是Aave仪表板

24
00:01:40,850 --> 00:01:45,575
这是一个具体头寸的截图

25
00:01:45,575 --> 00:01:50,880
所以我们有一个大约一万美金的抵押品

26
00:01:50,880 --> 00:01:57,200
这个人也借了大约1500美元的抵押贷款

27
00:01:57,600 --> 00:01:59,776
所以健康系数看起来相当不错，是吧

28
00:02:00,050 --> 00:02:02,640
它远远超过，呃……

29
00:02:02,640 --> 00:02:04,399
呃，高于1……

30
00:02:04,399 --> 00:02:08,925
所以我们有一个5.36的健康系数

31
00:02:09,325 --> 00:02:14,542
并且我们迄今只使用了一部分借款能力

32
00:02:15,120 --> 00:02:18,800
这个头寸还可以承担更多的债务

33
00:02:18,800 --> 00:02:20,800
如果这个人愿意的话

34
00:02:22,160 --> 00:02:23,120
那么

35
00:02:23,120 --> 00:02:28,475
如果我们看这个存款的具体元素

36
00:02:28,800 --> 00:02:31,360
这里有一笔Dai的存款是吧

37
00:02:31,360 --> 00:02:33,680
这是作为抵押品存入的Dai

38
00:02:33,680 --> 00:02:37,599
有趣之处在于，抵押品本身也被出借（给别人）

39
00:02:37,599 --> 00:02:39,519
所以借方可以赚取利息

40
00:02:39,519 --> 00:02:42,150
你可以通过这个按钮激活它

41
00:02:42,475 --> 00:02:47,550
这显然会触发了一个智能合约的交易

42
00:02:47,920 --> 00:02:54,427
激活了抵押品作为借出使用

43
00:02:54,800 --> 00:02:59,600
自然随这个收益而来的是清算的风险

44
00:03:00,480 --> 00:03:04,525
同学们还可以看到这个人借了一些Uni代币

45
00:03:04,525 --> 00:03:07,804
总共大约500个Uni代币

46
00:03:08,319 --> 00:03:10,800
借方需要支付利息

47
00:03:10,800 --> 00:03:14,319
这是借方需要支付的利息

48
00:03:14,319 --> 00:03:18,950
这在这个例中相当高，达到了113%

49
00:03:21,519 --> 00:03:24,804
正如我在本节课一开始提到的

50
00:03:25,120 --> 00:03:29,600
这个模型是被MakerDAO所使用

51
00:03:30,575 --> 00:03:32,175
或者说其由MakerDAO提出的

52
00:03:32,175 --> 00:03:36,448
那么，在第一步，借方提供了抵押品

53
00:03:36,448 --> 00:03:41,225
例如以太币，但现在也可以是各种不同的币

54
00:03:41,760 --> 00:03:44,800
所以现在，有（基于）多抵押品的Dai

55
00:03:44,800 --> 00:03:47,216
所以一旦你在这一步中提供了一些抵押品

56
00:03:47,216 --> 00:03:51,680
然后智能合约就会铸造Dai

57
00:03:51,680 --> 00:03:55,900
或者借方可以从特定智能合约中借出Dai

58
00:03:56,239 --> 00:03:59,900
这是借方可以自由操作的债务

59
00:04:00,400 --> 00:04:00,942
并且

60
00:04:01,439 --> 00:04:08,200
为了关闭这个CDP或者说抵押贷款头寸

61
00:04:08,480 --> 00:04:11,975
借款人可以偿还债务

62
00:04:11,975 --> 00:04:13,519
就是这个DAI，对吧

63
00:04:13,519 --> 00:04:15,280
所以一旦偿还DAI给了智能合约

64
00:04:15,280 --> 00:04:17,759
紧接着智能合约会解锁以太币

65
00:04:17,759 --> 00:04:22,300
这样借方就可以取回资金

66
00:04:24,320 --> 00:04:28,800
这些就是超额抵押借贷的最重要的基础

67
00:04:28,800 --> 00:04:32,476
这种借贷中你不能超过两倍的杠杆

68
00:04:32,720 --> 00:04:37,300
如果你对高杠杆感兴趣，请看看本课程关于欠额抵押的讲座

